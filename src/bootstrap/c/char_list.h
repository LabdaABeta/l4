#ifndef CHAR_LIST_H
#define CHAR_LIST_H

struct Char_List;

struct Char_List *new_char_list(void);
void append_char_list(struct Char_List *list, int next);
int length_char_list(const struct Char_List *list);
void dump_char_list(const struct Char_List *list, char *buffer);
void empty_char_list(struct Char_List *list);
int peek_last_char_list(const struct Char_List *list);
void free_char_list(struct Char_List *list);

#endif /* CHAR_LIST_H */
