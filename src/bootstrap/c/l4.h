#ifndef L4_H
#define L4_H

struct L4_State;

typedef void (*Printer)(int);

struct L4_State *new_state(Printer printer);
void consume_state(struct L4_State *state, int next);
void free_state(struct L4_State *state);

#endif /* L4_H */
