#include "l4.h"
#include <stdio.h>

void ignored_putchar(int ch) {
    putchar(ch);
}

int main(int argc, char *argv[]) {
    int ch;

    struct L4_State *state = new_state(ignored_putchar);

    while ((ch = getchar()) > 0)
        consume_state(state, ch);

    consume_state(state, ch);

    free_state(state);
}
