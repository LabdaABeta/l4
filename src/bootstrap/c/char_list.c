#include "char_list.h"

#include <stdlib.h>

struct Char_List_Node {
    int value;
    struct Char_List_Node *next;
};

struct Char_List {
    struct Char_List_Node *last;
    int length;
};

struct Char_List *new_char_list(void) {
    struct Char_List *result = malloc(sizeof(struct Char_List));
    result->last = 0;
    result->length = 0;
}

void append_char_list(struct Char_List *list, int next) {
    struct Char_List_Node *node = malloc(sizeof(struct Char_List_Node));
    node->value = next;
    node->next = list->last;

    list->last = node;
    list->length++;
}

int length_char_list(const struct Char_List *list) {
    return list->length;
}

void dump_char_list(const struct Char_List *list, char *buffer) {
    int index = list->length;
    struct Char_List_Node *node = list->last;
    buffer[index--] = 0;

    while (node) {
        buffer[index--] = node->value;
        node = node->next;
    }
}

int peek_last_char_list(const struct Char_List *list) {
    return list->length ? list->last->value : -1;
}

void empty_char_list(struct Char_List *list) {
    struct Char_List_Node *node;

    while (list->last) {
        node = list->last->next;
        free(list->last);
        list->last = node;
    }

    list->length = 0;
}

void free_char_list(struct Char_List *list) {
    empty_char_list(list);
    free(list);
}
