#include "l4.h"
#include "char_list.h"

#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <ctype.h>
#include <stdarg.h>

enum L4_Current_State {
    L4_CS_WHITESPACE,
    L4_CS_IDENTIFIER,
    L4_CS_OPERATOR,
    L4_CS_NUMBER, /* . is still operator (literal<1>.literal<5> = 1½ */

    L4_CS_MAX
};

enum L4_Character_Class {
    L4_CC_WHITESPACE,
    L4_CC_IDENTIFIER_CHAR,
    L4_CC_DIGIT,
    L4_CC_OPEN_BRACKET,
    L4_CC_CLOSE_BRACKET,
    L4_CC_OPEN_PARENTHESIS,
    L4_CC_CLOSE_PARENTHESIS,
    L4_CC_OPEN_BRACE,
    L4_CC_CLOSE_BRACE,
    L4_CC_QUOTE,
    L4_CC_OPERATOR,
    L4_CC_MAX
};

enum L4_Character_Class char_to_class(int ch) {
    if (iscntrl(ch))
        return L4_CC_MAX;
    else if (isspace(ch))
        return L4_CC_WHITESPACE;
    else if (isalpha(ch) || ch == '_')
        return L4_CC_IDENTIFIER_CHAR;
    else if (isdigit(ch))
        return L4_CC_DIGIT;
    else if (ch == '[')
        return L4_CC_OPEN_BRACKET;
    else if (ch == ']')
        return L4_CC_CLOSE_BRACKET;
    else if (ch == '(')
        return L4_CC_OPEN_PARENTHESIS;
    else if (ch == ')')
        return L4_CC_CLOSE_PARENTHESIS;
    else if (ch == '{')
        return L4_CC_OPEN_BRACE;
    else if (ch == '}')
        return L4_CC_CLOSE_BRACE;
    else if (ch == '"')
        return L4_CC_QUOTE;
    else
        return L4_CC_OPERATOR;
}

struct L4_State {
    struct Char_List *input_buffer;
    Printer printer;
    int eof;
    enum L4_Current_State current_state;
    char *comment_end;
};

struct L4_State *new_state(Printer printer) {
    struct L4_State *result = malloc(sizeof(struct L4_State));

    result->input_buffer = new_char_list();
    result->printer = printer;
    result->eof = 0;
    result->current_state = L4_CS_WHITESPACE;
    result->comment_end = 0;

    return result;
}

int l4_printf(Printer printer, const char *format, ...) {
    va_list args, args_copy;
    char *buf;
    int length;

    va_start(args, format);
    va_copy(args_copy, args);

    length = vsnprintf(0, 0, format, args);
    buf = alloca(length + 1);
    vsnprintf(buf, length + 1, format, args);

    va_end(args);
    va_end(args_copy);

    return length;
}

void consume_state(struct L4_State *state, int next) {
    if (next < 0) {
        state->eof = 1;
    } else {
        enum L4_Character_Class class = char_to_class(next);

        int last = peek_last_char_list(state->input_buffer);
        enum L4_Character_Class last_class = char_to_class(last);

        if (class == last_class) {
            append_char_list(state->input_buffer, next);
        } else {
            char *buf = alloca(length_char_list(state->input_buffer));
            dump_char_list(state->input_buffer, buf);
            l4_printf(state->printer, "%s\n", buf);
            empty_char_list(state->input_buffer);
            last_class = class;
        }
    }

    if (state->eof) {
        char *ch;
        char *buf = alloca(length_char_list(state->input_buffer));
        dump_char_list(state->input_buffer, buf);

        for (ch = buf; *ch; ch++)
            state->printer(*ch);
    }
}

void free_state(struct L4_State *state) {
    free_char_list(state->input_buffer);
    free(state);
}
