Terse syntax always looks like:
keyword Identifier(optional: type, arguments: other_type) {
    body
}

Verbose syntax is more variable



[generic](typed)?

by delaying `emit` calls code generation is delayed. Compiler just keeps running
functions until all trees end in bedrock (which must be implemented via emits)
