# L4

![logo](l4.svg "L4 Logo")

## Primary Design Goal

The primary goal of L4 is to be an **engineering-oriented general programming language**. This is accomplished via the following ideals:

 - **Control over compilation**: By providing control over how compilation works from end-to-end the entire process of the compiler can be inspected.

 - **Strong typing and interfacing**: By providing explicit limitations in all interfaces code is forced to think about requirements.

 - **Explicit static computation**: Can produce manually optimized binaries.

 - **Weightless interface definitions**: Can provide interfaces without affecting performance.

 - **Capable of cross-compiling to legible code**: Allows seamless integration with other programming languages.

 - **Minimal language definition**: By keeping the language itself simple and relying instead on standard libraries it is easier to isolate issues.

## Features

There are some additional features that are incidental to the primary design goal but are nonetheless useful.

 - **Separation of Libraries from Faces**: L4 has the concept of *faces*, files which define lexical elements rather than logical ones. These can have multiple implementations, much like fonts.

 - **Consistent Design**: Everything is a function call at compile time. Much of the syntax is effectively the result of taking operator overloading to the extreme. Even the assignment and declaration syntices are actually overloaded operators.

 - **Implicit Statement Separators**: Lines ending in either a close bracket (`)]}`) or an identifier have a semi-colon automatically appended if needed.

## Variables

A good example of how compilation is controlled manually is variable
declarations. The statement `val x : Integer = 10` is compiled as a static
function call to the `val` function with `x` as the identity parameter,
`Integer` as the type parameter and `{ return 10 }` as the block parameter.

This shows three types of syntactic sugar.

The first is that a function with no parameters does not require parentheses.
Otherwise it would have to be `val x() : Integer = 10`.

The second is that if the first argument of a function is an identifier, it can
be 'lifted' outside of the parentheses.
Otherwise it would have to be `val(x) : Integer = 10`.

[//]: # (This is wrong?, it should refer to :Type being a type param?)

The third is that even unitary types can have overloaded operators and that
operators can be any symbol. In fact, operators are just regular identifiers
with the only difference being that when next to 'traditional' identifiers no
whitespace is needed to separate them. In the above declaration `val x` is the
unary function `val` applied to the identifier argument `x`. The `:` is the
infix `:` function applied to the returned `Val_Declaration` and the type
`Integer`. Otherwise it would have to be `:(val x, Integer) = 10`.

Finally, the `= 10` is another infix function call to the `=` function that also
demonstrates the left-associative interpretations of operators. Otherwise it
would have to be `(val x : Integer) = 10`.

## Static vs Dynamic Divide

It is very easy to tell the difference between static and dynamic code. Static
code is all done in the type system, via type parameters and subtyping. For
example `Literal` is a built-in type with many built-in subtypes each with only
one legal value. For example `Literal[1]` is a type with only a single value,
which is the value `1`. Thus an optimized increment could be implemented by
overriding an increment function to take `Literal[1]` as different from
`Integer`.

In order to allow arbitrary literals there is a built-in block `computed` which
returns a literal version of whatever its block produces.

As an example consider the following code.


    val a : Integer = 1
    val b : Integer = 2

    \ Dynamic
    print(a + b)

    \ Static
    print(computed { a + b })


## Smart Casting

Another useful feature is smart casting. This is implemented by rewriting scope
in the implementation of branches. For example consider `if (My_Object.is_cool()) { ... }`.
One could implement in `is_cool()` a call to `Containing_If?.Register_Followed { variables(this@is_cool.name).type = Cool_Object_Type }`. Then if the branch is followed it would automatically cast `My_Object` to a `Cool_Object_Type`. Of course for this work properly `is_cool()` has to return a new subtype of `Boolean` that keeps track of negation and ors to be able to undo the registration. Additionally it is important to note that the `If_Block` that is returned by if has all registered followers `computed` to ensure the execution occurs at compile-time.

Of course other built-in functions make this process much easier for user-level code.

## Structure

L4 has a consistent structure. All L4 source files consist of a series of block constructs.

[//]: # (TODO: maybe all structure into syntactic sugar?)
[//]: # ( - If first argument is of type identifier ...)
[//]: # ( - If last argument is of type block ...)
[//]: # ( - Internal arguments can be of type expression, etc if necessary)

    block construct = Keyword, [ Identifier, [ arguments ] ], [ type result ], [ block body ] ;
    arguments = "(", argument, { ";", argument }, ")" ;
    argument = named argument | unnamed argument | assigned argument;
    unnamed argument = Type Expression ;
    type result = ":", Type Expression ;
    named argument = Identifier, ":", Type Expression ;
    block body = "{", {Statement}, "}" ;

These block constructs are run by the interpreter which will use them to add names to global state and produce code as necessary. It is this interpreter that does the typechecking and compilation.

Note that type expressions must resolve to a type and are evaluated at compile-time, not run-time. It is here, and in the implementation of each block construct, that static computation occurs. For example, in the following snippet, all *italic* text is run statically, while **bold** text is dynamic[^1]:

*fun foo(x : Int, y : Int) {* **return x + y** *}*

*type Percent(range = 0 .. 1, delta = 0.01)*

Note that even else statements are just a block structure. At compile time if statements 'save' their state so that else statements know which if statement they belong to.

## Faces

Faces determine constructs. They are searched in a manner similar to X logical font descriptions.

Faces define not just what block constructs do, but also the manner in which their contents are compiled. This includes compilation of statements and expressions as well as available block constructs.

Typically one would use the appropriate standard face files for your system and possibly build on them for unique additional use cases. However, when compiling for unique systems it may be necessary to re-implement the standard face files.

Face files are organized by fields:

 - **name**: the name of the symbol

 - **vendor**: the vendor of the face (e.g. std)

 - **target**: the target system for compilation

 - **optimization**: the level or form of optimization for the generated code

 - **debugging**: the level or form of debugging symbols to be included

 - **tooling**: the type of tooling information to be included (e.g. profiler)

The fields are `-` separated (e.g. `package-std-----` for the standard package).

## Libraries

[^1]: Or rather opaque to implementation. In reality all code is run 'statically', it is just that the code in the block is most likely to be compiled rather than executed, as it is not possible to fully examine it.
