# How L4 Compilation Works

Since most of L4's functionality comes from powerful built-in functions, the
compilation model is relatively simple.

## Comments

Comment processing happens before lexing. By default the comment character is
a backslash (`\`), the separator symbol is whitespace, and the line separator is
a new line.  These can be configured on the command-line, but it is typically
not advised to do so (it is provided mainly for DSL purposes).

Line comments consist of the comment character followed immediately by the
separator symbol and continue until the end of the line

Block comments consist of the comment character followed by any set of
characters followed by the separator symbol. They continue until the same set of
characters followed by the comment character appears again. They do not nest.

## Lexing

Lexing of L4 is extremely simple as there are no keywords to worry about (all
keywords are just built-in functions). The only relevant symbols are the paired
bracket symbols (`[](){}`) the quotation symbol (`"`) and digits (`0123456789`).

The quotation symbol toggles string mode where every character in the input is
treated as an operator (see Strings).

[//]: # (TODO: remember that 1.1e+10 is parsed as [1][.][1][e][+][10])

## Parsing

Parsing is done dynamically out of necessity, since the grammar changes on the
fly when functions are defined.

## Compiling

Compiling is done by effectively running a dynamic interpreter over the code.
This interpreter recursively 'expands' function calls until they all hit
bedrock. Bedrock is a special built-in function that triggers calls to the
selected implementation face for the given function.

Depending on the situation the compiler handles bedrock differently. In faces it
expands them using the implementation running on the current machine. In
specifications bedrock should not occur, and is not a defineable situation. In
implementations it expands them using the face specified on the command-line.
Other situations may use other forms of expansion (such as build files or
scripts).

Note that the only difference here is which face is used to compile the code
(local runner for faces, none for specifications, as specified for
implementations).

### Specifications

Specifications are compiled using a standard initial compiler state. Every
operation in the specification specifies modifications to be made to said state.
These modifications may be specifications of functions, variables, types, etc.
All symbols encountered in specifications can be considered as deep 'type'
variables.

For example, in a specification, `x : int` may create the symbol `x`, but it
will not store integer values but rather a complex data type containing such
information as its type, its name, its declaration location, and how it should
be declared.

The final state of the internal compiler state is then exported as the compiled
form of the specification, to be filled in by an implementation.

### Implementations

Implementations use the information specified in compiled specification files as
well as the implementation face selected by the compilation to convert a series
of operations into compiled code. It is in the implementation that one can think
of `x : int` creating a symbol `x` that stores an integer. The specification has
enough information to know that, for example, passing `x` as an `int` requires
sending only an integer, but passing `x` as an `any(int)` requires sending a tag
value as well.

### Faces

In faces variables must be thought of as they are in specifications. It is in
the face that the actual compiled instructions for the basic operations
specified as bedrock must be described. For example `x : int` may be compiled to
an n-bit integer value in the resulting code by the loaded face.

### Built-ins

Since specifications and faces both require interacting with the state of the
compiler, the compiler must provide some rudimentary built-in operations to be
able to store and retrieve information about the symbols in the code.

The most fundamental built-in is the `emit` built-in, which is used to emit
code.

Another fundamental built-in is the `'` operator which creates dynamically typed
static attributes of the name on the left with the name on the right. These
attributes can be assigned any value with `=`. For example `variable'value = 10`
sets the `value` field of `variable` to `10` in the compiler state. These
attributes are always resolved at compile time, as the `'` operator cannot be
overloaded. The `'` operator is also defined for unary names, in which case it
adds them to the unnamed global compiler state. This can be used to declare
compiler variables. For example `'var` declares a compiler state variable
called var.

Some attributes have pre-defined meanings to the compiler. For example
`a'invoke` is the function that is called when `a()` is compiled.

Note that all names created in the compiler state can have attributes attached
to them. These attributes can then be used by the face to compile the code.

As an example, consider the `var` function. It creates a variable. When
compiling for assembly the face may very well set `'address` for the created
symbol to indicate where it is stored in memory. Later implementations of `=`
could then reference `'address` and emit code in accordance with it.

[//]: # (TODO: will have to standardize and define the 'initial' state of the compiler)
[//]: # (Remember that the form that types take will be dependent on the specification)
[//]: # (not the language)


[//]: # (v {I like this idea better, personally, let ' be a normal operator!})
[//]: # (TODO: *or* is everything done using the type system? then its just
a matter of finding the right function to call... All optimization done as
overloaded functions then... try it?)
[//]: # (^)
